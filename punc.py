import re

class Punc: 
    def __init__(self):
        # check if punctuation at start
        self.pcheck=re.compile("^[.,?!:;-]")
    def getre(self):
        # get regular expression
        return self.pcheck
    def setre(self,r):
        # set regular expression
        self.pcheck=r
