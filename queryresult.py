import numpy as np
from scipy import spatial

tfidfff="./cacm/tfidf.dat"
queryff="./cacm/queryzeros.dat"
dcta={}
querylist=None
resultlist=[]

with open(tfidfff, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get list
            tmplstt=token.split(',')
            namef=tmplstt[0]
            tmplstt=tmplstt[1:]
        except ValueError:
            continue
        # make a list of count
        dcta[namef]=[itm for itm in tmplstt]

with open(queryff, 'r') as myfile:
    data=myfile.read()
    tmplist=data.split(',')
    querylist=[np.float32(i) for i in tmplist]

for key in dcta:
    # get vectors
    tmplist=[np.float32(i) for i in dcta[key]]
    if len(tmplist) <= 0:
        continue
    if len(querylist) <= 0:
        continue
    tmpresult=1-spatial.distance.cosine(tmplist, querylist)
    resultlist.append((key,tmpresult))

resultlist.sort(key=lambda x:x[1],reverse=True)

for item in resultlist:
    # print similarity
    print(item)
