import os
import math

docsroot="./cacm"
docsf="./cacm/documents.dat"
termsf="./cacm/index.dat"
data=None  
dataf=None  
dataterm=None  
namef=None  
namet=None  
idf=None  
idt=None  
tmplstt=None  
dct={}
listf=[]
listt=[]
listcount=[]
countt=0
docs=570

with open(docsf, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get name of file
            namef,idf=token.split(',')
        except ValueError:
            continue
        # make a list of files
        listf.append(namef)

with open(termsf, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get name of term
            namet,idt=token.split(',')
        except ValueError:
            continue
        # make a list of terms
        listt.append(namet)

for itemt in listt:
    # go through terms
    countt=0
    for itemff in listf:
        # go through files again
        with open(os.path.join(docsroot,itemff),'r') as myfile:
            # get file content
            data=myfile.read()
            if data.count(itemt) > 0:
                # term in file
                countt += 1
    # number of files for a term
    listcount.append(math.log(float(docs)/(float(countt+1))))

for itemf in listf:
    # go through files
    dct[itemf]=[itm for itm in listcount]
    dct[itemf].insert(0,itemf)

for key in dct:
    # print vectors
    tmplstt=dct[key]
    print(','.join(map(str,tmplstt)))
