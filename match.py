import re

def match(term,r):
    # match regular expression
    return r.match(term)

def matchle(term,ln):
    # match length
    return (len(term)<=ln)

def matchlst(term,r):
    # match regular expression list
    mtch=None
    for item in r:
        mtch=item.match(term)
        if mtch:
            return mtch
    return mtch
