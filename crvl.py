import requests
import lxml.html
import sys
import time
 
count=1
countpop=0
maxcount=50
stack=[]
t2 = time.localtime()   
stack.append(sys.argv[1])

while True:
    if len(stack)<=0:
        break

    url=stack.pop()
    rsp=requests.get(url)
    tree=lxml.html.fromstring(rsp.text)
    links=tree.cssselect('a')

    countpop+=1
    outfile = open('./crvl/crvl%s.html'%(countpop), 'w')
    outfile.write(rsp.text.encode('utf-8'))

    if count>=maxcount:
        continue

    for link in links:
        if 'href' in link.attrib:
            linkpath=link.attrib['href']
            if not linkpath.startswith('http'):
                continue
            if not linkpath.startswith('https'):
                continue
            count+=1
            stack.append(linkpath)

print 'Processing Start Time: %.2d:%.2d' % (t2.tm_hour, t2.tm_min)
print "Documents %i" % maxcount

t2 = time.localtime()   
print 'Processing End Time: %.2d:%.2d' % (t2.tm_hour, t2.tm_min)
