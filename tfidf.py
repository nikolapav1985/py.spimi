import os
import math

docsroot="./cacm"
docsf="./cacm/documents.dat"
countf="./cacm/count.dat"
dff="./cacm/df.dat"
data=None  
namef=None  
idf=None  
tmplstt=None  
tmplsttt=None  
dcta={}
dctb={}
dctc={}
listf=[]

with open(docsf, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get name of file
            namef,idf=token.split(',')
        except ValueError:
            continue
        # make a list of files
        listf.append(namef)

with open(countf, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get list
            tmplstt=token.split(',')
            namef=tmplstt[0]
            tmplstt=tmplstt[1:]
        except ValueError:
            continue
        # make a list of count
        dcta[namef]=[itm for itm in tmplstt]

with open(dff, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get list
            tmplstt=token.split(',')
            namef=tmplstt[0]
            tmplstt=tmplstt[1:]
        except ValueError:
            continue
        # make a list of count
        dctb[namef]=[itm for itm in tmplstt]

for key in dcta:
    # get vectors
    tmplstt=dcta[key]
    tmplsttt=dctb[key]
    dctc[key]=[]
    for tmpcount in range(len(tmplstt)):
        # get score
        dctc[key].append(float(tmplstt[tmpcount])*float(tmplsttt[tmpcount]))

for key in dctc:
    # print vectors
    tmplstt=dctc[key]
    tmplstt.insert(0,key)
    print(','.join(map(str,tmplstt)))
