Abstract
---------------------------------
  
- SPIMI, one of methods to index and search a file system. Especially useful for constrained devices. This includes things like single board computers and other IOT things.

- Like all indexing methods it is based on document vectors. Query is represented in the same manner. In order to resolve a query and get relevant documents similarity measure is computed. This should be easy to do for any query. Compute results, store and offer near instant response.
  
Single pass in memory index SPIMI
---------------------------------

- Test system Linux version 5.4.0-67-generic (buildd@lcy01-amd64-025) (gcc version 9.3.0 (Ubuntu 9.3.0-17ubuntu1~20.04)) #75-Ubuntu SMP Fri Feb 19 18:03:38 UTC 2021
- results are found in ./cacm directory
- results are found in ./crvl directory if running crawler
- cacm/documents.dat file contains a list of documents and ids
- cacm/index.dat file contains a list of terms and ids
- cacm/docf.dat document frequency per term
- cacm/docinvf.dat document inverse frequency per term
- cacm/termf.dat term frequency per document
- cacm/tfidf.dat tf-idf score for each document
- cacm/join.dat join token id document id
- cacm/count.dat count vector for each document
- cacm/df.dat df vector for each document
- cacm/queryzeros.dat example query
- cacm/queryresultzeros.dat example query results
- out.txt file contains metrics after running the program
- outquery.txt file contains results of example query
- outcrvl.txt file contains results of crawler run
- general description
- a program goes through a list of documents
- it adds a list of documents to a single file
- it adds a list of terms found in each document to a single file
- ignore.py terms to ignore
- number.py ignore numbers
- punc.py ignore punctuation at start
- match.py number of methods to do match
- porter.py porter stemmer
- ln.py ignore short strings
- query.py run simple query once index is constructed
- crvl.py run crawler
- ctr.py number of times a term is found in a document
- df.py number of documents for a term
- tfidf.py tf idf scores
- queryresult.py run query and use tf idf scores
- python indexer.py run script to get results
- python query.py Binomi Bisect run example query 
- [example data set](https://www.dropbox.com/s/95fxwisfez6ulxi/indexere.zip?dl=0)
- [description](https://towardsdatascience.com/tf-idf-for-document-ranking-from-scratch-in-python-on-real-world-dataset-796d339a4089)
- [project link](https://gitlab.com/nikolapav1985/py.spimi)
  
Brief description
---------------------------------
  
- based on vectors
- each document represented as a vector
- item in each vector is a term
- these are all terms found in a set of documents
- query is represented the same
- a similarity measure is computed for query and documents
- e.g. cosine similarity
- results are ranked top to bottom
- top results should be most relevant
- a cosine similarity (defined as a fraction, vector dot product over magnitude product)
- it is a measure in range 0 to 1
- 1 being most similar (kind of identical)
- each term has tf idf score computed (other scores are available too)
- tf score, count of term in a document over all terms in a document
- idf, inverse document frequency, number of documents over document frequency
- log of this can be used too
- document frequency, count of term in all documents
- tf idf score, tf times idf

About me
---------------------------------

- Currently involved in open source. Also maintaining some parts of Raspbian distribution (this being one of those parts). 

- Other than that background in system admin/system engineering. Mostly network environments. Starting from design, implementation and set up of basic network services.
