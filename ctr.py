import os

docsroot="./cacm"
docsf="./cacm/documents.dat"
termsf="./cacm/index.dat"
data=None  
dataf=None  
dataterm=None  
namef=None  
namet=None  
idf=None  
idt=None  
tmplstt=None  
dct={}

with open(docsf, 'r') as myfile:
    data=myfile.read()  
    for token in data.split('\n'):
        try:
            # get name of file
            namef,idf=token.split(',')
        except ValueError:
            continue
        # start a dictionary
        dct[namef]=[]
        with open(os.path.join(docsroot,namef),'r') as docfile:
            # get content of file
            dataf=docfile.read()
            with open(termsf,'r') as termsfile:
                # get terms
                dataterm=termsfile.read()
                for tokenterm in dataterm.split('\n'):
                    try:
                        # get term
                        namet,idt=tokenterm.split(',')
                    except ValueError:
                        continue
                        # term in doc
                    # number of times term appears in doc
                    dct[namef].append(dataf.count(namet))

for key in dct:
    # print vectors
    tmplstt=dct[key]
    tmplstt.insert(0,key)
    print(','.join(map(str,tmplstt)))
