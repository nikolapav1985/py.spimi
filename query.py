# run some query

import sys
import os

args = [item for item in sys.argv[1:]]
termf="./cacm/index.dat"
joinf="./cacm/join.dat"
docf="./cacm/documents.dat"
termfile=None
joinfile=None
docfile=None
termline=None
joinline=None
docline=None
termpart=None
joinpart=None
docpart=None
termid={}
termdocid={}

def termidput(item,termname,termno,termid):
    # assign term id
    if item==termname:
        termid[termname]=termno.rstrip("\n")

def termdocidput(key,termid,check,docid,termdocid):
    # join term id doc id
    if termid[key] == check:
        termdocid[(termid[key],docid)]=1

def docprint(docid,check,docname):
    # print matching document
    if docid == check:
        print(docname)

for item in args:
    termfile=open(termf,"r")
    for fline in termfile:
        termline=fline
        termpart=termline.split(",")
        termidput(item,termpart[0],termpart[1],termid)
    print(item)

for key in termid:
    # print term and id
    print(key)
    print(termid[key])

for key in termid:
    joinfile=open(joinf,"r")
    for fline in joinfile:
        joinline=fline
        joinpart=joinline.split(",")
        joinpart[0]=joinpart[0].rstrip("\n")
        joinpart[1]=joinpart[1].rstrip("\n")
        termdocidput(key,termid,joinpart[0],joinpart[1],termdocid)

for key in termdocid:
    # print term id doc id
    print(key[0])
    print(key[1])

for key in termdocid:
    docfile=open(docf,"r")
    print("Term id {}".format(key[0]))
    for fline in docfile:
        docline=fline
        docpart=docline.split(",")
        docpart[0]=docpart[0].rstrip("\n")
        docpart[1]=docpart[1].rstrip("\n")
        docprint(key[1],docpart[1],docpart[0])
