import re

class Number: 
    def __init__(self):
        # check if term is a number
        self.numcheck=re.compile("^[0-9]+$")
    def getre(self):
        # get regular expression
        return self.numcheck
    def setre(self,r):
        # set regular expression
        self.numcheck=r
