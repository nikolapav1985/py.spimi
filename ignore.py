import re

class Ignore: 
    def __init__(self):
        # check if term is ignored
        self.igcheck=[]
        self.igcheck.append(re.compile("^and$"))
        self.igcheck.append(re.compile("^or$"))
        self.igcheck.append(re.compile("^but$"))

        self.igcheck.append(re.compile("^else$"))
        self.igcheck.append(re.compile("^if$"))
        self.igcheck.append(re.compile("^here$"))

        self.igcheck.append(re.compile("^there$"))
        self.igcheck.append(re.compile("^is$"))
        self.igcheck.append(re.compile("^it$"))

        self.igcheck.append(re.compile("^up$"))
        self.igcheck.append(re.compile("^less$"))
        self.igcheck.append(re.compile("^more$"))

        self.igcheck.append(re.compile("^the$"))
        self.igcheck.append(re.compile("^at$"))
        self.igcheck.append(re.compile("^am$"))

        self.igcheck.append(re.compile("^pm$"))
        self.igcheck.append(re.compile("^big$"))
        self.igcheck.append(re.compile("^large$"))

        self.igcheck.append(re.compile("^small$"))
        self.igcheck.append(re.compile("^clear$"))
        self.igcheck.append(re.compile("^mess$"))

        self.igcheck.append(re.compile("^he$"))
        self.igcheck.append(re.compile("^him$"))
        self.igcheck.append(re.compile("^she$"))

        self.igcheck.append(re.compile("^they$"))
        self.igcheck.append(re.compile("^them$"))
        self.igcheck.append(re.compile("^zie$"))

        self.igcheck.append(re.compile("^sie$"))
        self.igcheck.append(re.compile("^ey$"))
        self.igcheck.append(re.compile("^ve$"))

        self.igcheck.append(re.compile("^tey$"))
        self.igcheck.append(re.compile("^e$"))
        self.igcheck.append(re.compile("^zim$"))

        self.igcheck.append(re.compile("^em$"))
        self.igcheck.append(re.compile("^ver$"))
        self.igcheck.append(re.compile("^ter$"))

        self.igcheck.append(re.compile("^zir$"))
        self.igcheck.append(re.compile("^hir$"))
        self.igcheck.append(re.compile("^eir$"))

        self.igcheck.append(re.compile("^vis$"))
        self.igcheck.append(re.compile("^tem$"))
        self.igcheck.append(re.compile("^zis$"))

        self.igcheck.append(re.compile("^hirs$"))
        self.igcheck.append(re.compile("^eirs$"))
        self.igcheck.append(re.compile("^vers$"))

        self.igcheck.append(re.compile("^ters$"))
        self.igcheck.append(re.compile("^zieself$"))
        self.igcheck.append(re.compile("^hirself$"))

        self.igcheck.append(re.compile("^eirself$"))
        self.igcheck.append(re.compile("^verself$"))
        self.igcheck.append(re.compile("^terself$"))
    def getre(self):
        # get regular expression
        return self.igcheck
    def setre(self,r):
        # set regular expression
        self.igcheck=r
